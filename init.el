;;; init.el --- Config my emacs


;;; Commentary:

;;; Code:
;; attributes for fonts
(add-to-list 'default-frame-alist
	     '(font . "Iosevka Term-12"))
(set-face-attribute 'default nil
		    :family "Iosevka Term"
		    :weight 'regular
		    :width 'normal)



;; Signature
(setq package-check-signature nil)

;; configuration file and exists emacs
(setq confirm-kill-emacs   'y-or-n-p
      mouse-yank-at-point t
      require-final-newline t
      custom-file "C:/Users/medua/AppData/Roaming/.emacs.d")

;; Initial function window
(defun mydefault-window-setup()
  "Called by Emacs startup-book to set up my initial window configuration."
  (split-window-right)
  (other-window 1)
  (find-file "c:/Users/medua/Documents/org/Tarefas.org")
  (other-window 1))

(add-hook 'emacs-startup-hook #'mydefault-window-setup)


;;Disable line number in mode text

(dolist (mode '(org-mode-hook
		markdown-mode-hooki
		dired-mode-hook))
  (add-hook mode(lambda () (display-line-numbers-mode 0))))


;; Spell TextMode
;; (add-to-list 'exec-path "C:/msys64/mingw64/bin/hunspell")
(setq ispell-program-name "hunspell")
(setq ispell-hunspell-dict-paths-alist
      '(("pt_BR" "C:/Users/medua/Downloads/veroptbrv320aoc/pt_BR.aff")))
(setq ispell-local-dictionary "pt_BR")
(setq ispell-local-dictonary-alist
      '(("pt_BR" "[[:alpha:]]" "[^[:alpha:]]" "[']" nil ("-d" "pt_BR") nil utf-8)))


(global-set-key (kbd "<f8>") 'ispell-word)
(global-set-key (kbd "C-S-<f8>") 'flyspell-mode)
(global-set-key (kbd "C-M-<f8>") 'flyspell-buffer)
(global-set-key (kbd "C-<f8>") 'flyspell-check-previous-highlighted-word)
	

(defun flyspell-check-next-highlighted-word ()
  "Custom function to spell next highlighted  word."
  (interactive)
  (flyspell-goto-next-error)
  (ispell-word))

(global-set-key (kbd "M-<f8>") 'flyspell-check-next-highlighted-word)



;; Keybindings Map

(defun merds/duplicate-line ()
  "Função para duplicar linhas."
  (interactive)
  (move-beginning-of-line 1)
  (kill-line)
  (yank)
  (forward-line 1)
  (yank))

(global-set-key (kbd "C-H d") 'merds/duplicate-line)

;;
(global-set-key (kbd "C-w") 'backward-kill-word)
(global-set-key (kbd "M-o") 'other-window)

;;
(defun merds/revert-two-buffer ()
  "Função que reverte os buffer e os fecha."
  (interactive)
  (delete-other-windows)
  (split-window-right))

(bind-key "C-x 1" #'merds/revert-two-buffer)
(bind-key "C-x !" #'delete-other-windows)


;; Move in Windows
(global-set-key (kbd "C-c <left>") 'windmove-left)
(global-set-key (kbd "C-c <right>") 'windmove-right)
(global-set-key (kbd "C-c <up>") 'windmove-up)
(global-set-key (kbd "C-c <down>") 'windmove-down)

;; Move line

(defun move-line-up ()
  "Move the current line is up."
  (interactive)
  (transpose-lines 1)
  (forward-line -2)
  (indent-according-to-mode))

(defun move-line-down ()
  "Move current line is down."
  (interactive)
  (forward-line 1)
  (transpose-lines 1)
  (forward-line -1)
  (indent-according-to-mode))

(global-set-key (kbd "M-<up>") #'move-line-up)
(global-set-key (kbd "M-<down>") #'move-line-down)

;; Encoding

(prefer-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)


;;disable tool bar
(menu-bar-mode -1)
(when (fboundp 'tool-bar-mode)
  (tool-bar-mode -1))

;;Scroll disable
(when (fboundp 'scroll-bar-mode)
  (scroll-bar-mode -1))


;; Line Number
(column-number-mode)
(global-display-line-numbers-mode t)
(setq display-line-numbers-type 'relative)


;; Paren mode
(show-paren-mode 1)

;;Theme defaults

(require-theme 'modus-themes)
(setq modus-themes-italic-constructs t
      modus-themes-bold-constructs t)

(load-theme 'modus-vivendi :no-confirm)

(define-key global-map (kbd "<f5>") #'modus-themes-toggle)

;;; Straight

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 6))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))


(setq package-enable-at-startup nil)

(straight-use-package 'use-package)


;; Use-package
(require 'package)
(setq package-name-column-width 40
      package-version-column-width 14
      package-status-column-width 12
      package-archive-column-width 8)

(add-hook 'package-menu-mode-hook #'hl-line-mode)

(setq package-archives
      '(("elpa"   . "https://elpa.gnu.org/packages/")
	("elpa-devel" . "https://elpa.gnu.org/devel/")
	;("nongnu" . "https//elpa.nongnu.org/nongnu/")
	("melpa" . "https://melpa.org/packages/")))

(setq package-archive-priorities
      '(("elpa" . 2)
	("melpa".  1)))

(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(eval-and-compile
  (setq use-package-always-ensure t
        use-package-expand-minimally t))


;;;; Configuration for Editor

;; Rainbow Delimiters
 (use-package rainbow-delimiters
   :init
   (rainbow-delimiters-mode)
   :config
   (add-hook 'prog-mode-hook #'rainbow-delimiters-mode))
	
;; Autopair
(use-package smartparens
  :config
  (smartparens-global-mode 1))

(use-package corral
  :init)


(setq corral-preserve-point t)




;;;Modeline
(use-package all-the-icons
  :if (display-graphic-p))

(use-package doom-modeline
  :ensure t
  :init (doom-modeline-mode 1)
  :config(setq doom-modeline-icon t
	       doom-modeline-major-mode-icon t
	       doom-modeline-major-mode-color-icon t
	       doom-modeline-buffer-state-icon t
	       doom-modeline-buffer-modification-icon t
	       doom-modeline-bar-width 3
	       doom-modeline-github t
	       doom-modeline-minor-modes t))

;; Mutiple-Cursor

(use-package multiple-cursors
  :ensure t
  :bind (("C-=" . mc/edit-lines)
	 ("C->" . mc/mark-next-like-this)
	 ("C-<" . mc/previous-like-this)
	 ("C-c C-<" . mc/mark-all-like-this)))

;; Buffer
(use-package bufler
  :disabled
  :bind
  ("C-x b" . #'bufler-switch-buffer)
  ("C-x B" . #'counsel-switch-buffer))

;; Fussy Search
(use-package fussy
  :ensure t
  :config
  (setq fussy-filter-fn 'fussy-filter-default)
  (setq fussy-use-cache t)
  (setq fussy-compare-same-score-fn 'fussy-histlen->strlen<)

  (push 'fussy completion-styles)
  (setq completion-category-defaults nil
	completion-category-overrides nil))


;;; Helm

(use-package helm
  :demand t
  :bind(("M-x" . helm-M-x)
	("C-x C-f" . helm-find-files)
	("C-c b" . helm-buffers-list)
	("C-c o" . helm-occur)
	("C-h SPC" . helm-all-mark-rings)
	("C-c g" . helm-google-suggest)
	("C-c r" . helm-register))
  
  :config
  ;; (require 'helm-autoloads)
  (setq helm-split-window-inside-p           t
	helm-move-to-line-cycle-in-source     t
	helm-ff-search-library-in-sexp        t
	helm-scroll-amount                    8
	helm-ff-file-name-history-use-recentf t
	helm-echo-input-in-header-line t
	helm-fuzzy-match-fn t
	helm-apropos-fuzzy-match t
	helm-semantic-fuzzy-match t
	helm-imenu-fuzzy-match t
	helm-locate-fuzzy-match t
	helm-mode-fuzzy-match t)

  ;; (require 'helm-descbinds)
  ;; (helm-descbinds-mode)
  
  ;; (require 'helm-config)
  (setq helm-move-to-line-cycle-in-source t)

  (semantic-mode 1)
  (helm-mode 1))

(define-key helm-map (kbd "C-i") 'helm-execute-persistent-action)
(define-key helm-map (kbd "C-c z")  'helm-select-action)

(when (executable-find "ack")
  (setq helm-grep-default-command "ack -Hn --no-group --no-color %e %p %f"
	helm-grep-default-recurse-command "ack -H --no-group --no-color %e %p %f"))

;; Projectile(gerenciar projetos)


(use-package projectile
  :init
  :config
  (projectile-mode 1))

(define-key projectile-mode-map (kbd "C-;") 'projectile-command-map)

(setq projectile-completion-system 'helm)

(use-package helm-projectile
  :config (helm-projectile-on))



;; fzf
(use-package fzf :ensure t)
;; Invoke-Fzf and Ps-fzf (Windows)
;; Get-ChildItem . -Recurse -Attributes !Directory | Invoke-Fzf | % { buffer $_ }



;;;  Dimmer

;; Dim inactive windows
;; (use-package dimmer
(use-package dimmer
  :init
  :config
  (dimmer-mode t)
  (setq dimmer-fraction 0.5)
  (setq dimmer-adjustment-mode :foreground)
  (setq dimmer-use-colorspace :rgb)
  (setq dimmer-watch-frame-focus-events nil)
  (dimmer-configure-which-key)
  (dimmer-configure-magit)
  (dimmer-configure-posframe))



;;;; Configuration Org/Markdown(Text-mode)

(use-package org-modern
  :ensure t)



(defun merds/orgmode-setup ()
  "Função de predefinições do Orgmode."
  (variable-pitch-mode 0)
  (auto-fill-mode 1)
  (visual-line-mode 1)
  (org-modern-mode)
  (org-modern-agenda)
  (setq truncate-lines nil
	org-auto-align-tags nil
	org-tags-column 0
	org-catch-invisible-edits 'show-and-error
	org-special-ctrl-a/e t
	org-insert-heading-respect-content t
	org-pretty-entities t)
  (modify-all-frames-parameters
   '((right-divider-width . 3)
     (internal-border-width . 3)))
  (setq-default fill-column 3000))

(dolist (face '(window-divider
		window-divider-first-pixel
		window-divider-last-pixel))
  (face-spec-reset-face face)
  (set-face-foreground face (face-attribute 'default :background)))
(set-face-background 'fringe (face-attribute 'default :background))

(setq org-agenda-tags-column 0
      org-agenda-block-separator ?-
      org-agenda-time-grid '((daily today require-timed)
			     (800 1000 1200 1400 1600 1800 2000)
			     "______" "_________________")
      org-agenda-current-time-string
      "<----- now -----------------------------------------------------------")


(use-package org
  :hook(org-mode . merds/orgmode-setup)
  :config
  (setq org-ellipsis "⤵"
	org-hide-emphasis-markers t)
  
  (setq org-directory "C:/Users/medua/Documents/org/"
	org-default-notes-file (concat org-directory "C:/Users/medua/Documents/org/"))
  (setq org-agenda-files (list "C:/Users/medua/Documents/org/Tarefas.org"))
  :bind(
	("C-c L" . org-stored-links)
	("C-c a" . org-agenda)))

(use-package org-bullets
  :after org
  :hook(org-mode . org-bullets-mode))

(setq org-hide-leading-stars t
      org-startup-indented t)

;; (setq org-file-apps (append
;; 		     '(("\\.pdf\\'" . "Foxit PDF Reader %s")
;; 		       ("\\.x?html?\\'" . "Microsoft Edge %s")
;; 		       ) org-file-apps))


;; Org-Roam

(use-package org-roam
  :ensure t
  :init
  ;; (setq org-roam-v2-ack t)
  :custom
  (org-roam-directory (file-truename "C:/Users/medua/Documents/org/roam/"))
  (org-roam-completion-everywhere t)
  (org-roam-file-extesions '("org" "md"))
  (org-roam-dailies-capture-templates
   '(("i" "default" entry "* %<%I:%M %p>: %?"
      :if-new (file+head "%<%d-%m-%Y>.org" "#+title: %<%d-%m-%Y>\n"))))
  :bind (("C-c n l" . org-roam-buffer-toggle)
	 ("C-c n f" . org-roam-node-find)
	 ("C-c n i" . org-roam-node-insert)
	 ("C-c n c" . org-roam-capture)
	 ("C-c n o" . org-roam-dailies-map)
	 ("C-c n g" . org-roam-graph))
  (:map org-mode-map
	("C-M-i" . completion-at-point))
  (:map org-roam-dailies-map
	("I" . org-roam-dailies-capture-today)
	("Y" . org-roam-dailies-capture-yesterday)
	("T" . org-roam-dailies-capture-tomorrow))
  :bind-keymap
  :config
  (require 'org-roam-dailies)
  (org-roam-db-autosync-mode)
  (require 'org-roam-protocol))


;; Bibliography
(use-package citar
  :custom
  (citar-bibliography '("c:/Users/medua/OneDrive/Documentos/Estudos/Work_Latex/bib/Computação.bib"))
  :hook
  (LaTeX-mode . citar-capf-setup)
  (org-mode . citar-capf-setup)
  :config
  :bind (:map org-mode-map
	      :package org("C-c i" . #'org-cite-insert)))


(setq citar-templates '((main . "${author editor:30%sn}   ${date year issued:4}   ${title:48}")
			(suffix . "    ${=key= id:15}   ${=type=:12}   ${tags keywords:*}"   )
			(preview . "${author editor:%etal} (${year issued date}) ${title}, ${journal journaltitle publisher container-title collection-title}.\n")
			(note .  "Notes on ${author editor:%%etal}, ${title}")))




(use-package citar-org-roam
  :after (citar org-roam)
  :config (citar-org-roam-mode))

(setq citar-org-roam-note-title-template "${author} - ${title}")

(setq org-roam-capture-templates
      '(("d" "default" plain "%?"
	 :target (file+head "%<%d%m%Y%H%M%S>-${slug}.org"
			    "#+title:${note-title}\n")
	 :unnarrowed t)
	("n" "literature note" plain "%?"
	 :target (file+head "%(expand-file-name (or citar-org-roam-subdir "") org-roam-directory)/${citar-citekey}.org"
			    "#+title: ${citar-citekey} (${citar-date}).${note-title}.\n#+created: %U\n#+last_modified:%U\n\n")
	 :unnarrowed t)))



(setq helm-bibtex-bibliography '("C:/Users/medua/OneDrive/Documentos/Estudos/Work_Latex/bib"))
(setq helm-bibtex-library-path '("c:/Users/medua/OneDrive/Documentos/Estudos/Work_Latex/"))




;;; Pdf Tools

;; (use-package org-pdftools
;;   :hook(org-mode . org-pdftools-setup-link))


;; Markdown


(use-package markdown-mode
  :ensure t
  :hook (markdown-mode . dw/org-mode-setup)
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode))
  :init (setq markdown-command "multimarkdown"))


(autoload 'markdown-mode "markdown-mode"
   "Major mode for editing Markdown files" t)
(add-to-list 'auto-mode-alist
             '("\\.\\(?:md\\|markdown\\|mkd\\|mdown\\|mkdn\\|mdwn\\)\\'" . markdown-mode))

(autoload 'gfm-mode "markdown-mode"
   "Major mode for editing GitHub Flavored Markdown files" t)
(add-to-list 'auto-mode-alist '("README\\.md\\'" . gfm-mode))


(setq markdown-asymmetric-header t)
(setq markdown-unordered-list-item-prefix "*   ")
(setq markdown-italic-underscore t)
(setq markdown-bold-underscore nil)
(setq markdown-spaces-after-code-fence 0)
(setq markdown-footnote-location 'subtree)
(setq markdown-enable-wiki-links t)
(setq markdown-link-space-sub-char "-")
(setq markdown-wiki-link-fontify-missing t)
(setq markdown-wiki-link-fontify-missing t)
(setq markdown-split-window-direction 'right)
	

;; Use visual-line-mode in gfm-mode

(defun my-gfm-mode-hook ()
  "Função para visualização do Markdown."
  (visual-line-mode 1))
(add-hook 'gfm-mode-hook 'my-gfm-mode-hook)

;;; Latex

;; (use-package tex
;;   :ensure auctex
;;   :config(require 'tex-mik))


;;; LaTeX
(defvar latex--company-backends nil)

(use-package tex
  :straight nil
  :ensure auctex
  :mode ("\\.tex\\'" . LaTeX-mode)
  :custom
  (TeX-auto-save t) ; parse on save
  (TeX-parse-self t) ; parse on load
  ;; hidden dirs for auctex files
  (TeX-auto-local ".auctex-auto")
  (TeX-style-local ".auctex-style")
  (TeX-syntactic-comment t)
  ;; Synctex Support
  (TeX-source-correlate-mode t)
  (TeX-source-correlate-method 'synctex)
  (TeX-source-correlate-start-server nil)
  ;; Don't insert line-break at inline math
  (TeX-electric-sub-and-superscript t)
  (LaTeX-fill-break-at-separators nil)
  ;; (TeX-view-program-list '(("zathura" "zathura --page=%(outpage) %o")))
  ;; (TeX-view-program-selection '((output-pdf "zathura")))
  :config
  (setq-default TeX-engine 'luatex)
  (add-hook 'TeX-mode-hook (lambda ()
                             (setq-local ispell-parser 'tex)
                             (setq-local fill-nobreak-predicate (cons #'texmathp fill-nobreak-predicate))))
  (add-hook 'TeX-mode-hook #'visual-line-mode)
  (add-hook 'TeX-mode-hook #'TeX-fold-mode))




(use-package cdlatex
  :hook (LaTeX-mode . cdlatex-mode)
  :custom
  (cdlatex-use-dollar-to-ensure-math nil)
  :bind
  (:map cdlatex-mode-map
        (("$" . nil)
         ("(" . nil)
         ("{" . nil)
         ("[" . nil)
         ("|" . nil)
         ("<" . nil)
         ("TAB" . nil)
         ("^". nil)
         ("_" . nil))))

(use-package adaptive-wrap
  :hook (LaTeX-mode . adaptive-wrap-prefix-mode)
  :init (setq-default adaptive-wrap-extra-indent 0))

(use-package auctex-latexmk
  :after latex
  :init
  (setq auctex-latexmk-inherit-TeX-PDF-mode t)
  (add-hook 'LaTeX-mode-hook
            (lambda ()
              (setq-local TeX-command-default "LatexMk")))
  :config
  (auctex-latexmk-setup))

(use-package company-auctex
  :defer t
  :init
  (add-to-list 'latex--company-backends #'company-auctex-environments nil #'eq)
  (add-to-list 'latex--company-backends #'company-auctex-macros nil #'eq))

(use-package company-math
  :defer t
  :init
  (add-to-list 'latex--company-backends #'latex-symbols-company-backend nil #'eq))

;;;; RefTex

(use-package reftex
  :straight nil
  :hook (LaTeX-mode . reftex-mode)
  :config
  (setq reftex-cite-format
        '((?a . "\\autocite[]{%l}")
          (?b . "\\blockquote[]{%l}")
          (?c . "\\cite[]{%l}")
          (?f . "\\footcite[]{%l}")
          (?n . "\\nocite{%l}")
          (?p . "\\parencite{%l}")
          (?s . "\\smartcite[]{%l}")
          (?t . "\\textcite[]{%l}"))
        reftex-plug-into-AUCTeX t
        reftex-toc-split-windows-fraction 0.3))

(use-package company-reftex
  :config
  (add-hook 'reftex-mode-hook
            (lambda ()
              (setq-local company-backends (cons 'company-reftex company-backends)))))


;; lsp -latex

(use-package lsp-ltex
  :ensure t
  :hook (LaTeX-mode . (lambda ()
			(require 'lsp-ltex)
			(lsp-deferred)))  ; or lsp-deferred
  :init
  (setq lsp-ltex-version "15.2.0")
  :config
  (setq lsp-ltex-language "pt-BR"))  ; make sure you have set this, see below



;;; Presentations


;; TODO(MARCO)config: Reveal.js



;;; Language Configuration


;; Extra Packages

(use-package aggressive-indent)

(add-hook 'emacs-lisp-mode-hook #'aggressive-indent-mode)
(add-hook 'css-mode-hook #'aggressive-indent-mode)
;; (add-hook 'python-mode-hook #'aggressive-indent-mode)

;; Gitconfig


(use-package magit
  :init
  :hook(magit-mode . hl-line-mode))

;; Configs best coding

;; Commenting lines
(use-package evil-nerd-commenter
  :bind ("M-/" . evilnc-comment-or-uncomment-lines))

(put 'scroll-left 'disabled nil)

(global-set-key (kbd "M-;") 'evilnc-copy-to-line)
(global-set-key (kbd "C-c l") 'evilnc-comment-or-uncomment-region)
(global-set-key (kbd "C-c c") 'evilnc-copy-and-comment-lines)
(global-set-key (kbd "C-c p") 'evilnc-comment-or-uncomment-paragraphs)
	
;;; EmacsTree-sitter
(use-package tree-sitter
  :init
  :hook ((js-mode . tree-sitter-hl-mode)
         (go-mode . tree-sitter-hl-mode)
	 (typescript-mode . treesitter-hl-mode)))


(global-tree-sitter-mode)
(add-hook 'tree-sitter-after-on-hook #'tree-sitter-hl-mode)

;; (use-package tree-sitter-langs)




;; Lsp-Mode
;;
;;

(use-package lsp-mode
  :commands (lsp lsp-execute-code-action)
  :init
  (setq lsp-keymap-prefix "C-c l")  ;; Or 'C-l', 's-l'
  :hook((go-mode . lsp-deferred)
	(clojure-mode . lsp-mode)
        ;; (lsp-mode . lsp-diagnostic-modeline-mode)
        (lsp-mode . lsp-ui-mode)
	(lsp-mode . lsp-enable-which-key-integration))
  :bind ("C-c C-c" . #'lsp-execute-code-action)
  :config
  (setq lsp-modeline-diagnostics-enable t
	lsp-modeline-code-actions-enable t
        lsp-enable-symbol-highlighting t
        lsp-lens-enable t)

  :custom
  (lsp-print-performance t)
  ;;(lsp-diagnostics-modeline-mode-scope :project)
  (lsp-log-io t)
  (lsp-file-watch-threshold 5000)
  (lsp-enable-file-watchers nil)
  (lsp-rust-analyzer-cargo-watch-command "clippy")
  (lsp-eldoc-render-all t)
  (lsp-idle-delay 0.6)
  ;; enable / disable the hints as you prefer:
  (lsp-rust-analyzer-server-display-inlay-hints t)
  (lsp-rust-analyzer-display-lifetime-elision-hints-enable "skip_trivial")
  (lsp-rust-analyzer-display-chaining-hints t)
  (lsp-rust-analyzer-display-lifetime-elision-hints-use-parameter-names nil)
  (lsp-rust-analyzer-display-closure-return-type-hints t)
  (lsp-rust-analyzer-display-parameter-hints nil)
  (lsp-rust-analyzer-display-reborrow-hints nil))
	

;; Lsp-UI

(use-package lsp-ui
  :ensure
  :commands lsp-ui-mode
  :custom
  (lsp-ui-doc-delay 0.75)
  (lsp-ui-doc-max-height 200)
  (lsp-ui-peek-always-show t)
  (lsp-ui-sideline-show-hover t)
  (lsp-ui-doc-enable nil)
  (setq lsp-ui-doc-position 'bottom)
  ;; Sideline(lsp-ui)
  (setq lsp-ui-sideline-enable t)
  (setq lsp-ui-sideline-show-hover t)
  :hook (lsp-mode . lsp-ui-mode)
  :after lsp-mode)


;; treemacs (lsp)
(use-package lsp-treemacs
  :after lsp)
	

;; Ivy (lsp)
	
(use-package lsp-ivy
  :after (ivy lsp-mode))
	

;; Completions (Company-mode)

(use-package company
  :after lsp-mode
  :hook (prog-mode . company-mode)
  :bind (:map company-active-map)
  ("<tab>" . company-complete-selection)
  (:map lsp-mode-map)
  ("<tab>" . company-indent-or-complete-common)
  :custom
  (company-minimum-prefix-length 1)
  (company-idle-delay 0.0))
	

(use-package company-box
  :hook (company-mode . company-box-mode))

;; Check lint

(use-package flycheck
  :ensure t
  :init (global-flycheck-mode))



;; Lang Typescript

(defun merds/setup-tide-mode ()
  "Função de Configuração do Typescript."
  (interactive)
  (tide-setup)
  (flycheck-mode 1)
  (setq flycheck-check-syntax-automatically '(save mode-enable))
  (eldoc-mode 1)
  (lsp-mode 1)
  (tide-hl-identifier-mode 1)
  (company-mode 1))
	  

(use-package tide
  :ensure t
  :hook ((tide-mode . merds/setup-tide-mode)))
(setq company-tooltip-align-annotations t)
(add-hook 'before-save-hook 'tide-format-before-save)

(setq tide-format-options '(:insertSpaceAfterFunctionKeywordForAnonymousFunctions t :placeOpenBraceOnNewLineForFunctions nil))


(use-package web-mode)
(add-to-list 'auto-mode-alist '("\\.tsx\\'" . web-mode))
(add-hook 'web-mode-hook (lambda ()
			   (when (string-equal "tsx"
                                  (file-name-extension buffer-file-name))
	                     (merds/setup-tide-mode))))
(flycheck-add-mode 'typescript-tslint 'web-mode)

(add-hook 'typescript-mode-hook 'lsp-deferred)
(add-hook 'javascript-mode-hook 'lsp-deferred)

(add-hook 'tsx-ts-mode-hook #'merds/setup-tide-mode)


;;; C/C++ Config

(use-package ccls
  :ensure t
  :config
  (setq ccls-executable "ccls")
  (setq lsp-prefer-flymake nil)
  (setq-default flycheck-disabled-checkers '(c/c++-clang c/c++-cppcheck c/c++-gcc))
  :hook ((c-mode c++-mode objc-mode) .
         (lambda () (require 'ccls) (lsp))))



;;; Python-mode

(use-package python
  :mode ("\\.py" . python-mode)
  :config
  (use-package elpy
    :init
    (add-to-list 'auto-mode-alist '("\\.py$" . python-mode))
    :config
    (setq elpy-rpc-backend "jedi")
    (add-hook 'python-mode-hook 'py-autopep8-enable-on-save)
    ;; flycheck-python-flake8-executable "/usr/local/bin/flake8"
    :bind (:map elpy-mode-map)
    ("M-." . elpy-goto-definition)
    ("M-," . pop-tag-mark))
  (elpy-enable))

(use-package pip-requirements
  :config
  (add-hook 'pip-requirements-mode-hook #'pip-requirements-auto-complete-setup))

(setq flycheck-python-pylint-executable "pylint")

(use-package lsp-pyright
  :config
  (setq lsp-clients-python-command "pyright")
  :hook (python-mode . (lambda ()
	                 (require 'lsp-pyright)
	                 (lsp))))

;; (use-package lsp-python-ms
;;   :ensure t
;;   :hook (python-mode . (lambda ()
;;                          (require 'lsp-python-ms)
;;                          (lsp)))
;;   :init
;;   (setq lsp-python-ms-executable (executable-find "python-language-server")))



(use-package poetry
  :ensure t)

;;; Go-mode


(use-package go-mode
  :ensure t
  :hook (go-mode . (lambda ()
		     (setq tab-width 4))))





(provide 'init)
;;; init.el ends here
